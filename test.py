from neo4j.v1 import GraphDatabase
from html_helper import find_tx_given_token_contract_address_and_account_address
driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "gelei"))
driver_session = driver.session()

def delete_all_data(tx):
    tx.run("MATCH (n) DETACH DELETE n")

# Delete all the data in db
#driver_session.write_transaction(delete_all_data)

def add_account_tx(tx, from_address, from_address_name,to_address,to_address_name,amount):
    tx.run("MERGE (a:Account {address: $from_address,name:$from_address_name}) "
           "MERGE (a)-[:transfer {amount:$amount}]->(account:Account {address: $to_address,name:$to_address_name})",
           from_address=from_address, from_address_name=from_address_name,to_address=to_address,to_address_name=to_address_name,amount=amount)



def check_for_address_name(address):
    if address in exchange_account:
        return exchange_account[address]
    else:
        return address

def save_tx_to_db(tx_arrs,root_account):
    receipt_amount = dict()
    non_exchange_accounts = set()

    for tx in tx_arrs:
        from_address,tx_type,to_address,quantity = tx
        if tx_type == "OUT":
            if to_address not in receipt_amount:
                receipt_amount[to_address] = quantity
            else:
                receipt_amount[to_address] += quantity
    for to_address in receipt_amount:
        to_address_name = check_for_address_name(to_address)
        from_address_name = check_for_address_name(root_account)
        driver_session.write_transaction(add_account_tx,root_account, from_address_name,to_address, to_address_name,receipt_amount[to_address])

        if to_address_name == to_address:
            non_exchange_accounts.add(to_address)
    return non_exchange_accounts

def recurse_bfs(non_exchange_accounts,current_depth,max_depth=10):
    if current_depth >= max_depth:
        return
    current_depth += 1
    new_non_exchange_accounts = set()
    for non_exchange_receipt in non_exchange_accounts:
        tx_arrs = find_tx_given_token_contract_address_and_account_address(kyber_address,non_exchange_receipt)
        tmp_sets = save_tx_to_db(tx_arrs,non_exchange_receipt)
        new_non_exchange_accounts = new_non_exchange_accounts.union(tmp_sets)
    recurse_bfs(new_non_exchange_accounts,current_depth)

fbg_address = "0x69ea6b31ef305d6b99bb2d4c9d99456fa108b02a"
account_address = "0x8f0cc7844edcbe997716c0790b05a2e9ba3401e0"
# account_address = "0x8d12a197cb00d4747a1fe03395095ce2a5cc6819"
kyber_address = "0xe41d2489571d322189246dafa5ebde1f4699f498"
exchange_account = {"0x5a94af9f0bde9cd49d86de1bd2f1f3db3929c42f":"yubo_liqui_io_account","0x8d12a197cb00d4747a1fe03395095ce2a5cc6819":"etherdelta_2","0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be":"binance"}
tx_arrs = find_tx_given_token_contract_address_and_account_address(kyber_address,account_address)
print("tx_arrs:{}".format(tx_arrs))
non_exchange_accounts = save_tx_to_db(tx_arrs,account_address)
print("non_exchange_accounts:{}".format(non_exchange_accounts))
recurse_bfs(non_exchange_accounts,1)
driver_session.close()
